<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210524134021 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE discount CHANGE discount_date discount_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE item_purchased ADD number_of_item INT NOT NULL, CHANGE item_date item_date DATETIME NOT NULL, CHANGE items_bought items_id INT NOT NULL');
        $this->addSql('ALTER TABLE items CHANGE iteminsertdate iteminsertdate DATETIME NOT NULL');
        $this->addSql('ALTER TABLE shipping CHANGE shipping_date shipping_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE shipping_company CHANGE companydate companydate DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE discount CHANGE discount_date discount_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE item_purchased ADD items_bought INT NOT NULL, DROP items_id, DROP number_of_item, CHANGE item_date item_date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE items CHANGE iteminsertdate iteminsertdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE shipping CHANGE shipping_date shipping_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE shipping_company CHANGE companydate companydate DATETIME NOT NULL');
    }
}
