<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210524125425 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE order_status (id INT AUTO_INCREMENT NOT NULL, statusname VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shipping (id INT AUTO_INCREMENT NOT NULL, shipping_code VARCHAR(255) NOT NULL, tracking_number VARCHAR(255) NOT NULL, shipping_company_code VARCHAR(255) NOT NULL, shipping_amt NUMERIC(10, 2) NOT NULL, shipping_address VARCHAR(255) NOT NULL, shipping_date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shipping_company (id INT AUTO_INCREMENT NOT NULL, companycode VARCHAR(255) NOT NULL, companyaddress VARCHAR(255) DEFAULT NULL, companyname VARCHAR(255) NOT NULL, companydate DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE discount CHANGE discount_date discount_date DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE order_status');
        $this->addSql('DROP TABLE shipping');
        $this->addSql('DROP TABLE shipping_company');
        $this->addSql('ALTER TABLE discount CHANGE discount_date discount_date DATETIME NOT NULL');
    }
}
