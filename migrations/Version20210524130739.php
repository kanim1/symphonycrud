<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210524130739 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE discount CHANGE discount_date discount_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE orders ADD order_by_id INT NOT NULL, ADD order_status_id INT NOT NULL, DROP order_total, DROP order_discount, DROP order_item_id, DROP shipping_id');
        $this->addSql('ALTER TABLE shipping CHANGE shipping_date shipping_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE shipping_company CHANGE companydate companydate DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE discount CHANGE discount_date discount_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE orders ADD order_total NUMERIC(10, 0) NOT NULL, ADD order_discount NUMERIC(10, 0) NOT NULL, ADD order_item_id INT NOT NULL, ADD shipping_id INT NOT NULL, DROP order_by_id, DROP order_status_id');
        $this->addSql('ALTER TABLE shipping CHANGE shipping_date shipping_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE shipping_company CHANGE companydate companydate DATETIME NOT NULL');
    }
}
