<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210525111406 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE movie (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE discount CHANGE discount_date discount_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE items CHANGE iteminsertdate iteminsertdate DATETIME NOT NULL');
        $this->addSql('ALTER TABLE shipping CHANGE shipping_date shipping_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE shipping_company CHANGE companydate companydate DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE movie');
        $this->addSql('ALTER TABLE discount CHANGE discount_date discount_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE items CHANGE iteminsertdate iteminsertdate DATETIME NOT NULL');
        $this->addSql('ALTER TABLE shipping CHANGE shipping_date shipping_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE shipping_company CHANGE companydate companydate DATETIME NOT NULL');
    }
}
