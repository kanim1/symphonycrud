<?php

namespace App\Entity;

use App\Repository\ShippingRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ShippingRepository::class)
 */
class Shipping
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shipping_code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tracking_number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shipping_company_code;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $shipping_amt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shipping_address;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $shipping_date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShippingCode(): ?string
    {
        return $this->shipping_code;
    }

    public function setShippingCode(string $shipping_code): self
    {
        $this->shipping_code = $shipping_code;

        return $this;
    }

    public function getTrackingNumber(): ?string
    {
        return $this->tracking_number;
    }

    public function setTrackingNumber(string $tracking_number): self
    {
        $this->tracking_number = $tracking_number;

        return $this;
    }

    public function getShippingCompanyCode(): ?string
    {
        return $this->shipping_company_code;
    }

    public function setShippingCompanyCode(string $shipping_company_code): self
    {
        $this->shipping_company_code = $shipping_company_code;

        return $this;
    }

    public function getShippingAmt(): ?string
    {
        return $this->shipping_amt;
    }

    public function setShippingAmt(string $shipping_amt): self
    {
        $this->shipping_amt = $shipping_amt;

        return $this;
    }

    public function getShippingAddress(): ?string
    {
        return $this->shipping_address;
    }

    public function setShippingAddress(string $shipping_address): self
    {
        $this->shipping_address = $shipping_address;

        return $this;
    }

    public function getShippingDate(): ?\DateTimeInterface
    {
        return $this->shipping_date;
    }

    public function setShippingDate(\DateTimeInterface $shipping_date): self
    {
        $this->shipping_date = $shipping_date;

        return $this;
    }
}
