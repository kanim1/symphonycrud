<?php
namespace App\Entity;

use App\Repository\CustomerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $customername;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $customeremail;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $customermobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customeraddress;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomername(): ?string
    {
        return $this->customername;
    }

    public function setCustomername(string $customername): self
    {
        $this->customername = $customername;

        return $this;
    }

    public function getCustomeremail(): ?string
    {
        return $this->customeremail;
    }

    public function setCustomeremail(string $customeremail): self
    {
        $this->customeremail = $customeremail;

        return $this;
    }

    public function getCustomermobile(): ?string
    {
        return $this->customermobile;
    }

    public function setCustomermobile(?string $customermobile): self
    {
        $this->customermobile = $customermobile;

        return $this;
    }

    public function getCustomeraddress(): ?string
    {
        return $this->customeraddress;
    }

    public function setCustomeraddress(?string $customeraddress): self
    {
        $this->customeraddress = $customeraddress;

        return $this;
    }
}
