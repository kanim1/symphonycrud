<?php

namespace App\Entity;

use App\Repository\ShippingCompanyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ShippingCompanyRepository::class)
 */
class ShippingCompany
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $companycode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyaddress;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $companyname;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $companydate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompanycode(): ?string
    {
        return $this->companycode;
    }

    public function setCompanycode(string $companycode): self
    {
        $this->companycode = $companycode;

        return $this;
    }

    public function getCompanyaddress(): ?string
    {
        return $this->companyaddress;
    }

    public function setCompanyaddress(?string $companyaddress): self
    {
        $this->companyaddress = $companyaddress;

        return $this;
    }

    public function getCompanyname(): ?string
    {
        return $this->companyname;
    }

    public function setCompanyname(string $companyname): self
    {
        $this->companyname = $companyname;

        return $this;
    }

    public function getCompanydate(): ?\DateTimeInterface
    {
        return $this->companydate;
    }

    public function setCompanydate(\DateTimeInterface $companydate): self
    {
        $this->companydate = $companydate;

        return $this;
    }
}
