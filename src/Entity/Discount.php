<?php

namespace App\Entity;

use App\Repository\DiscountRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DiscountRepository::class)
 */
class Discount
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $fk_order_id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $discount_amt;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $discount_date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFkOrderId(): ?int
    {
        return $this->fk_order_id;
    }

    public function setFkOrderId(int $fk_order_id): self
    {
        $this->fk_order_id = $fk_order_id;

        return $this;
    }

    public function getDiscountAmt(): ?string
    {
        return $this->discount_amt;
    }

    public function setDiscountAmt(string $discount_amt): self
    {
        $this->discount_amt = $discount_amt;

        return $this;
    }

    public function getDiscountDate(): ?\DateTimeInterface
    {
        return $this->discount_date;
    }

    public function setDiscountDate(\DateTimeInterface $discount_date): self
    {
        $this->discount_date = $discount_date;

        return $this;
    }
}
