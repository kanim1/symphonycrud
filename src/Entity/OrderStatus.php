<?php

namespace App\Entity;

use App\Repository\OrderStatusRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderStatusRepository::class)
 */
class OrderStatus
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $statusname;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatusname(): ?string
    {
        return $this->statusname;
    }

    public function setStatusname(string $statusname): self
    {
        $this->statusname = $statusname;

        return $this;
    }
}
