<?php

namespace App\Entity;

use App\Repository\ItemsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ItemsRepository::class)
 */
class Items
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $itemname;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $itemamt;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $itempurchaseprice;

    /**
     * @ORM\Column(type="integer")
     */
    private $itemremain;

    /**
     * @ORM\Column(type="integer")
     */
    private $itemsold;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $iteminsertdate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItemname(): ?string
    {
        return $this->itemname;
    }

    public function setItemname(string $itemname): self
    {
        $this->itemname = $itemname;

        return $this;
    }

    public function getItemamt(): ?string
    {
        return $this->itemamt;
    }

    public function setItemamt(string $itemamt): self
    {
        $this->itemamt = $itemamt;

        return $this;
    }

    public function getItempurchaseprice(): ?string
    {
        return $this->itempurchaseprice;
    }

    public function setItempurchaseprice(string $itempurchaseprice): self
    {
        $this->itempurchaseprice = $itempurchaseprice;

        return $this;
    }

    public function getItemremain(): ?int
    {
        return $this->itemremain;
    }

    public function setItemremain(int $itemremain): self
    {
        $this->itemremain = $itemremain;

        return $this;
    }

    public function getItemsold(): ?int
    {
        return $this->itemsold;
    }

    public function setItemsold(int $itemsold): self
    {
        $this->itemsold = $itemsold;

        return $this;
    }

    public function getIteminsertdate(): ?\DateTimeInterface
    {
        return $this->iteminsertdate;
    }

    public function setIteminsertdate(\DateTimeInterface $iteminsertdate): self
    {
        $this->iteminsertdate = $iteminsertdate;

        return $this;
    }
}
