<?php

namespace App\Entity;

use App\Repository\ItemPurchasedRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ItemPurchasedRepository::class)
 */
class ItemPurchased
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;



    /**
     * @ORM\Column(type="datetime")
     */
    private $item_date;


      /**
     * @ORM\Column(type="integer")
     */
    private $items_id;

     /**
     * @ORM\Column(type="integer")
     */
    private $number_of_item;


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getItemDate(): ?\DateTimeInterface
    {
        return $this->item_date;
    }

    public function setItemDate(\DateTimeInterface $item_date): self
    {
        $this->item_date = $item_date;

        return $this;
    }

    public function getItemID(): ?int
    {
        return $this->items_id;
    }

    public function setItemID(int $items_id): self
    {
        $this->items_id = $items_id;
        return $this;
    }
    public function getItemNumberofItem(): ?int
    {
        return $this->number_of_item;
    }

    public function setItemNumberofItem(int $number_of_item): self
    {
        $this->number_of_item= $number_of_item;
        return $this;
    }

    
}
