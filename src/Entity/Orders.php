<?php

namespace App\Entity;

use App\Repository\OrdersRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrdersRepository::class)
 */
class Orders
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="integer")
     */
    private $order_by_id;


    /**
     * @ORM\Column(type="integer")
     */
    private $order_status_id;


    

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getOrderStatusID(): ?int
    {
        return $this->order_status_id;
    }

    public function setOrderStatusID(int $order_status_id): self
    {
        $this->order_status_id = $order_status_id;
        return $this;
    }

    public function getOrderByID(): ?int
    {
        return $this->order_by_id;
    }

    public function setOrderByID(int $order_by_id): self
    {
        $this->order_by_id = $order_by_id;
        return $this;
    }
    
    
}
