<?php

namespace App\Controller;

use App\Entity\Customer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Repository;
    /**
     * @Route("/api/customer", name="postss")
     */
class CustomerController extends AbstractController
{
    /**
     * @Route("/customer", name="customer")
     */
    
    public function index(): Response
    {
        return $this->render('customer/index.html.twig', [
            'controller_name' => 'CustomerController',
        ]);
    }

    /**
     * @Route("/save", name="add_customer", methods={"POST"})
     */

    public function save(Request $request): JsonResponse
    {
          $customers = new Customer();
           $data = json_decode($request->getContent(), true);
           $entityManager=$this->getDoctrine()->getManager();
         
           
           $customers->setCustomername($data['cname']);
           $customers->setCustomeremail($data['email']);
           $customers->setCustomermobile($data['mobile']);
           $customers->setCustomeraddress($data['address']);
           
           // tell Doctrine you want to (eventually) save the Product (no queries yet)
       $entityManager->persist($customers);

       // actually executes the queries (i.e. the INSERT query)
       $entityManager->flush();

       //return new Response('Saved new product with id '.$customername);
        if(!empty($customers->getId)){
            return new JsonResponse(['status' => 'Successfully Created','statusid'=>'1000'], Response::HTTP_CREATED);
        }else{
            return new JsonResponse(['status' => 'Customer Was Created','statusid'=>'4000'], Response::HTTP_CREATED);
        }
       
    }

    /**
    * @Route("/search/{id}", name="get_one_customer", methods={"GET"})
    */
    public function search($id)
    {

        
    $customer = $this->getDoctrine()
            ->getRepository(Customer::class)
            ->find($id);
            $data=[
                "id"=>$customer->getId(),
                "customername"=>$customer->getCustomername(),
                "customeremail"=>$customer->getCustomeremail(),
                "customermobile"=>$customer->getCustomermobile(),
                "customeraddress"=>$customer->getCustomeraddress(),
            ];

            
            return new JsonResponse($data, Response::HTTP_CREATED);
        //return new Response('Saved new product with id'.$customer->getCustomername());
        //return $this->render('product/show.html.twig', ['product' => $product]);
    }

}
