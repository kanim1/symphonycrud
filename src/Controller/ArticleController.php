<?php 
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArticleController extends AbstractController {
    /**
     * @Route("/", methods={"GET"})
     */
    public function index() 
    {
        $articles= ['Emmanuel Anim'];
        $profession= ['Web developer'];
       //return new Response('<html><body>Hello world</body></html>');    
        return $this->render('articles/index.html.twig',array('articles'=>$articles,'profession'=>$profession));
    }

}