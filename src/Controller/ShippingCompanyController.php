<?php

namespace App\Controller;

use App\Entity\ShippingCompany;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShippingCompanyController extends AbstractController
{
    /**
     * @Route("/shipping/company", name="shipping_company")
     */
    public function index(): Response
    {
        return $this->render('shipping_company/index.html.twig', [
            'controller_name' => 'ShippingCompanyController',
        ]);
    }

    /**
     * @Route("/Shppingcompany/save", name="order")
     */

    public function save()
    {
           $entityManager=$this->getDoctrine()->getManager();
           $shippingc = new ShippingCompany();
           $shippingc->setCompanycode('OO2');
           $shippingc->setCompanyaddress('ACCRA MAIN STREET');
           $shippingc->setCompanyname('UPS');
           
           // tell Doctrine you want to (eventually) save the Product (no queries yet)
       $entityManager->persist($shippingc);

       // actually executes the queries (i.e. the INSERT query)
       $entityManager->flush();

       return new Response('Saved new product with id '.$shippingc->getId());
    }

}
