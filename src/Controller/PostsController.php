<?php

namespace App\Controller;

use App\Entity\Posts;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

    /**
     * @Route("/api/postss", name="postss")
     */
class PostsController extends AbstractController
{
    /**
     * @Route("/posts", name="posts")
     */
    public function index(): Response
    {
        return $this->render('posts/index.html.twig', [
            'controller_name' => 'PostsController',
        ]);
    }

    /**
     * @Route("/testing", name="postss")
     */
    public function testing()
    {
     
        $entityManager=$this->getDoctrine()->getManager();
        $posts = new Posts();
        $posts->setTitle('Adidas Shoe');
        
        // tell Doctrine you want to (eventually) save the Product (no queries yet)
    $entityManager->persist($posts);

    // actually executes the queries (i.e. the INSERT query)
    $entityManager->flush();

    return new Response('Saved new product with id '.$posts->getId());
    }
}
