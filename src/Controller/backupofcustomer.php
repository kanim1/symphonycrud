<?php

namespace App\Controller;

use App\Entity\Customer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class backupofcustomer extends AbstractController
{
    /**
     * @Route("/customer", name="customer")
     */
    public function index(): Response
    {
        return $this->render('customer/index.html.twig', [
            'controller_name' => 'CustomerController',
        ]);
    }

    /**
     * @Route("/Customer/save", name="customer")
     */

    public function save()
    {
           $entityManager=$this->getDoctrine()->getManager();
           $customers = new Customer();
           $customers->setCustomername('Kwasi Anim');
           $customers->setCustomeremail('kwasi@gmail.com');
           $customers->setCustomermobile('0507480050');
           $customers->setCustomeraddress('No. 25 Independence Avenu, Accra-Ghana');
           
           // tell Doctrine you want to (eventually) save the Product (no queries yet)
       $entityManager->persist($customers);

       // actually executes the queries (i.e. the INSERT query)
       $entityManager->flush();

       return new Response('Saved new product with id '.$customers->getId());
    }

}
