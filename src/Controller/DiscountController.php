<?php

namespace App\Controller;

use App\Entity\Discount;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DiscountController extends AbstractController
{
    /**
     * @Route("/discount", name="discount")
     */
    public function index(): Response
    {
        return $this->render('discount/index.html.twig', [
            'controller_name' => 'DiscountController',
        ]);
    }
    /**
     * @Route("/Discount/save", name="discountamunt")
     */
    public function save()
    {
           $entityManager=$this->getDoctrine()->getManager();
           $discounts = new Discount();
           $discounts->setFkOrderId(1);
           $discounts->setDiscountAmt(40);
           
           // tell Doctrine you want to (eventually) save the Product (no queries yet)
       $entityManager->persist($discounts);

       // actually executes the queries (i.e. the INSERT query)
       $entityManager->flush();

       return new Response('Saved new product with id '.$discounts->getId());
    }
}
