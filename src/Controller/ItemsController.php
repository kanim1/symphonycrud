<?php

namespace App\Controller;

use App\Entity\Items;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ItemsController extends AbstractController
{
    /**
     * @Route("/items", name="items")
     */
    public function index(): Response
    {
        return $this->render('items/index.html.twig', [
            'controller_name' => 'ItemsController',
        ]);
    }


    /**
     * @Route("/Items/save", name="items")
     */
    public function save()
    {
           $entityManager=$this->getDoctrine()->getManager();
           $items = new Items();
           $items->setItemname('Adidas Shoe');
           $items->setItemamt(500);
           $items->setItempurchaseprice(450);
           $items->setItemremain(30);
           $items->setItemsold(3);
           
           // tell Doctrine you want to (eventually) save the Product (no queries yet)
       $entityManager->persist($items);

       // actually executes the queries (i.e. the INSERT query)
       $entityManager->flush();

       return new Response('Saved new product with id '.$items->getId());
    }

     /**
     * @Route("/Items/new", name="items")
     */
    public function new(): Response
    {
          // $entityManager=$this->getDoctrine()->getManager();
   
       return $this->render('items/new.html.twig');
    }

    /**
     * @Route("/Items/show", name="items")
     */
    public function show(): Response
    {
          // $entityManager=$this->getDoctrine()->getManager();
   
       return $this->render('items/new.html.twig');
    }

    /**
     * @Route("/Items/edit", name="items")
     */
    public function edit(): Response
    {
          // $entityManager=$this->getDoctrine()->getManager();
   
       return $this->render('items/new.html.twig');
    }
}
