<?php

namespace App\Controller;

use App\Entity\Orders;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * @Route("/order", name="order")
     */
    public function index(): Response
    {
        return $this->render('order/index.html.twig', [
            'controller_name' => 'OrderController',
        ]);
    }

    /**
     * @Route("/order/save", name="order")
     */

     public function save()
     {
            $entityManager=$this->getDoctrine()->getManager();
            $orders = new orders();
            $orders->setOrderStatusID(2);
            $orders->setOrderByID(1);
            
            // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($orders);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        return new Response('Saved new product with id '.$orders->getId());
     }


}
