<?php

namespace App\Controller;

use App\Entity\Shipping;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShippingController extends AbstractController
{
    /**
     * @Route("/shipping", name="shipping")
     */
    public function index(): Response
    {
        return $this->render('shipping/index.html.twig', [
            'controller_name' => 'ShippingController',
        ]);
    }

    /**
     * @Route("/shipping/save")
     */
    public function save()
    {
           $entityManager=$this->getDoctrine()->getManager();
           $shipping = new Shipping();
           $shipping->setShippingCode('OO1');
           $shipping->setTrackingNumber('00001');
           $shipping->setShippingAmt(20);
           $shipping->setShippingAddress('Tamale House number 123');
           $shipping->setShippingCompanyCode('001');
           
           // tell Doctrine you want to (eventually) save the Product (no queries yet)
       $entityManager->persist($shipping);

       // actually executes the queries (i.e. the INSERT query)
       $entityManager->flush();

       return new Response('Saved new product with id '.$shipping->getId());
    }

}
