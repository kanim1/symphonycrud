<?php

namespace App\Controller;

use\App\Entity\ItemPurchased;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ItemPurchasedController extends AbstractController
{
    /**
     * @Route("/item/purchased", name="item_purchased")
     */
    public function index(): Response
    {
        return $this->render('item_purchased/index.html.twig', [
            'controller_name' => 'ItemPurchasedController',
        ]);
    }

    /**
     * @Route("/itempurchased/save", name="item_purchased")
     */
    public function save()
    {
           $entityManager=$this->getDoctrine()->getManager();
           $items = new ItemPurchased();
           $items->setItemID(3);
           $items->setItemNumberofItem(1);
           
           // tell Doctrine you want to (eventually) save the Product (no queries yet)
       $entityManager->persist($items);

       // actually executes the queries (i.e. the INSERT query)
       $entityManager->flush();

       return new Response('Saved new product with id '.$items->getId());
    }
}
