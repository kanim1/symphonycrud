<?php

namespace App\Controller;

use App\Entity\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

 
class PostController extends AbstractController
{
    /**
     * @Route("/post", name="post")
     */
    public function index(): Response
    {
        return $this->render('post/index.html.twig', [
            'controller_name' => 'PostController',
        ]);
    }

    /**
    * @Route("/create", name="create", methods={"POST"})
    */
        public function create(Request $request)
        {
            $entityManager=$this->getDoctrine()->getManager();
            //$data = json_decode($request->getContent(), true);
            //create new post obj.
            $posts = new Post();
            //$title=$request->query->get('title');
            $posts->setTitle("hhhj");
            //instantiate the entity manager
               //$this->getDoctrine()->getManager();
            //save post to database
            //$em->persist($posts);
            //$em->flush();
            //return response to client
            return new Response($posts);
        }

         /**
    * @Route("/testingbb", name="testing", methods={"POST"})
    */
    public function testing()
    {
        $entityManager=$this->getDoctrine()->getManager();
        $posts = new Post();
        $posts->setTitle("hhhj");
 
        return new Response($posts);
    }
}
